from equipement.models import Equipement, Incident
from badge.models import Badge
from rest_framework import serializers
from django.contrib.auth.models import User


class EquipementSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Equipement
        fields = ('id', 'nom', 'description')


class IncidentsSerializer(serializers.HyperlinkedModelSerializer):
    equipement = EquipementSerializer(read_only=True)

    class Meta:
        model = Incident
        fields = ('id', 'description', 'statut', 'niveau', 'nom', 'equipement')


class BadgeSerializer(serializers.HyperlinkedModelSerializer):


    class Meta:
        model = Badge
        fields = ('id', 'numero_tag', 'active', 'user_id' )
