from api.serializers import EquipementSerializer, IncidentsSerializer, BadgeSerializer
from equipement.models import Equipement, Incident
from badge.models import Badge
from rest_framework import status
from rest_framework import generics, request
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
import json
from django.shortcuts import render, redirect, get_object_or_404
import webbrowser
from django.contrib.auth.models import User
import requests


class EquipementListCreate(generics.ListCreateAPIView):
    queryset = Equipement.objects.all()
    serializer_class = EquipementSerializer


class EquipementDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Equipement.objects.all()
    serializer_class = EquipementSerializer


class IncidentsListCreate(generics.ListCreateAPIView):
    queryset = Incident.objects.all()
    serializer_class = IncidentsSerializer


class IncidentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Incident.objects.all()
    serializer_class = IncidentsSerializer


class BadgeListCreate(generics.ListCreateAPIView):
    queryset = Badge.objects.all()
    serializer_class = BadgeSerializer


class BadgeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Badge.objects.all()
    serializer_class = BadgeSerializer


@csrf_exempt
def check_equipement(request):
    user = ''
    habilitation_utilisateur = ''
    categorie_habilitation = ''
    habi_mini = ''
    # si la method est post

    if request.method == 'POST':

        # on recupere le corps de la requete

        data = request.body

        # on utilise json.loads pour transformer la chaine de charcacteres en dictionnaire
        donnees_recues = json.loads(data)

        tag = donnees_recues['numero_tag']
        print(tag)
        equipement_recu = donnees_recues['id_equipement']
        print(equipement_recu)

        try:
            badge_user = Badge.objects.get(numero_tag=tag)
        except:
            response_data = {}
            response_data['access'] = 'False'
            response_data['message'] = "Votre badge n'est pas reconnu"
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        try:
            user = User.objects.get(id=badge_user.user_id)  # R�cupere le user du badge
        except:
            response_data = {}
            response_data['access'] = 'False'
            response_data['message'] = "Ce badge n'appartient a personne "
            return HttpResponse(json.dumps(response_data), content_type="application/json")


        equipement = Equipement.objects.get(id=equipement_recu)  # recupere l'equipement li� au nodemcu

        categorie = equipement.categorie

        categorie_habilitation = CategorieEquipement.objects.get(nom=categorie)

        try:
            habilitation_utilisateur = HabiitationUtilisateur.objects.get(
                utilisateur=user.id)  # je recupere l'habilitation du user

        except HabiitationUtilisateur.DoesNotExist:
            response_data = {}
            response_data['access'] = 'False'
            response_data['message'] = "Bonjour " + user.username + " Vous n'avez aucune habilitation pour utiliser cette machine ! "
            return HttpResponse(json.dumps(response_data), content_type="application/json")


        #habi_mini = (dict(HabiitationUtilisateur.habilitations)[habilitation_utilisateur.habilitation])
        print(habilitation_utilisateur.habilitation)
        print(categorie_habilitation.habilitation_mini.niveau)


        response_data = {}
        response_data['access'] = 'true'
        if (user == habilitation_utilisateur.utilisateur and str( habilitation_utilisateur.categorie) == categorie_habilitation.nom and int(habilitation_utilisateur.habilitation)  >= categorie_habilitation.habilitation_mini.niveau):
            response_data['message'] = "Bonjour " + user.username + " Bienvenue et bon travail !"
        else:
            response_data['message'] = "Bonjour " + user.username + " Vous n'avez pas une habilitation suffisante pour utiliser cette machine"



        return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def check_reception(request):



    # si la method est post

    if request.method == 'POST':


        # on recupere le corps de la requete


        data = request.body

        # on utilise json.loads pour transformer la chaine de charcacteres en dictionnaire
        donnees_recues = json.loads(data)

        tag = donnees_recues['numero_tag']

        user = None

        BadgeConnu  = False

        BadgeSansUser = False

        BadgeAvecUser = False




        try:

            badge_user = Badge.objects.get(numero_tag=tag) # recupere le badge avec le numero tag
            BadgeConnu = True






        except:
            badge_user = None
            BadgeConnu = False


        try:
            user = User.objects.get(id=badge_user.user_id)  # recupere le user du badge
            BadgeAvecUser = True


        except:
            BadgeSansUser = True



        if BadgeConnu == True & BadgeSansUser == True:
            url1 = "".join(['http://', request.META['HTTP_HOST'], '/equipement/creation?numero_tag=', tag])

        elif BadgeConnu == True & BadgeAvecUser == True:
            url1 = "".join(['http://', request.META['HTTP_HOST'], '/equipement/success?user=', user.username])

        elif BadgeConnu == False:
            url2 = "".join(['http://', request.META['HTTP_HOST'], '/equipement/creation?numero_tag=', tag])






        if (BadgeConnu == True):

            response_data = {}
            response_data['access'] = 'true'

            response_data['lien'] = url1

        else:
            response_data = {}
            response_data['access'] = 'False'
            response_data['message'] = "Ce badge n'existe pas cliquez sur le lien pour l'ajoutez"
            response_data['lien'] = url2


        return HttpResponse(json.dumps(response_data), content_type="application/json")





    if request.method == 'GET':
        return HttpResponse(200)
