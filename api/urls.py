from django.urls import path
from django.conf.urls import url

from . import views


urlpatterns = [
    path('equipement/', views.EquipementListCreate.as_view()),
    path('incidents/', views.IncidentsListCreate.as_view()),
    url(r'^equipement/(?P<pk>[0-9]+)/$', views.EquipementDetail.as_view()),
    url(r'^badge/(?P<pk>[0-9]+)/$', views.BadgeDetail.as_view()),
    url(r'^incident/(?P<pk>[0-9]+)/$', views.IncidentDetail.as_view()),
    url (r'^check/equipement', views.check_equipement),
    url (r'^check/reception', views.check_reception),
    path('badge/', views.BadgeListCreate.as_view()),

]
