from django.db import models





class Habilitation(models.Model):

    niveau = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return self.niveau
