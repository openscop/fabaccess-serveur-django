from django.contrib import admin
from .models import Incident, Equipement, CategorieEquipement, HabiitationUtilisateur

admin.site.register(Equipement)
admin.site.register(CategorieEquipement)
admin.site.register(HabiitationUtilisateur)

@admin.register(Incident)
class IncidentsAdmin(admin.ModelAdmin):
    readonly_fields = ['date']
    list_display = 'pk', 'date', 'statut', 'niveau', 'equipement', 'user'
    list_filter = 'statut', 'niveau', 'equipement'
