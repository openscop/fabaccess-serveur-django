# Generated by Django 2.0.5 on 2018-05-17 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('equipement', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='incidents',
            name='niveau',
            field=models.CharField(choices=[('BAS', 'Bas'), ('MOYEN', 'Moyen'), ('ELEVE', 'Élevé')], default=2, max_length=6),
            preserve_default=False,
        ),
    ]
