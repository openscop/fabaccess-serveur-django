from django.shortcuts import render, redirect, reverse
from .models import Equipement
from badge.models import Badge
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from .forms import BadgeCreateForm


def home(request):
    return render(request, 'equipement/home.html')


def all_equipement(request):
    equipements = Equipement.objects.all()
    return render(request, 'equipement/all_equipement.html', {'equipements': equipements})


def notif(request):
    return render(request, 'equipement/notif.html')


@csrf_exempt
def success(request):
    nom = ''

    if request.method == 'GET':
        nom = request.GET['user']

    context = {

        'message': "Ce badge est connu mais souhaitez vous le rattachez a un utilisateur ?",
        'username': nom,

    }
    return render(request, 'equipement/success.html', context)



def creation(request):

    tag = request.GET['numero_tag']


    users = User.objects.all()

    form = BadgeCreateForm(request.POST)

    if request.method == 'POST':




        if form.is_valid():
            badge = form.save(commit=False)
            badge.numero_tag = tag
            badge.save()
            return redirect(reverse(home))

    return render(request, 'equipement/creation.html',{'form': form, 'users': users, 'tag': tag})



def add_badge(request):

    user = User.objects.all()

    return render(request, 'equipement/add_badge.html', {'user': user})


def account(request):
    username = request.POST['username']
    password = request.POST['password']

    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)

        return redirect(reverse(home))
    else:

        error = "L'identifiant ou le mot de passe est incorrect "

        return render(request, 'equipement/home.html', {'errors': error})
