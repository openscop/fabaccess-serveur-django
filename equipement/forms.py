from django  import forms

from badge.models import Badge

class BadgeCreateForm(forms.ModelForm):

    class Meta:
        model = Badge
        fields = ('user',)
