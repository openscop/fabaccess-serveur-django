from django.conf.urls import url
from equipement import views

urlpatterns = [

    url(r'^/?$', views.all_equipement, name='all'),
    url(r'^success/?$', views.success, name='success'),
    url(r'^creation/?$', views.creation, name='creation'),


    url(r'^add_badge/(?P<id>\d+)/$', views.add_badge, name='add_badge'),
    url(r'^account/', views.account, name='account'),

]
