from django.db import models
from django.contrib.auth.models import User
from core.models.habilitation import Habilitation


class Incident(models.Model):

    NIVEAUX = (
        ('0', 'Amélioration'),
        ('1', 'Non bloquant'),
        ('2', 'Bloquant')
    )
    STATUT = (
        ('0', 'Nouveau'),
        ('1', 'En cours de qualification'),
        ('2', 'En cours de traitement'),
        ('3', 'Résolu'),
    )

    date = models.DateTimeField("Date et heure de 1ère observation", auto_now=True, )
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    equipement = models.ForeignKey('Equipement', on_delete=models.CASCADE)
    description = models.TextField()
    niveau = models.CharField(max_length=15, choices=NIVEAUX)
    statut = models.CharField(max_length=50, choices=STATUT)
    suivi = models.TextField("Suivi du traitement", blank=True, help_text="Détail qui, quand, comment l'incident est traité")

    def __str__(self):
        return self.description[30:] + "..."


class CategorieEquipement(models.Model):
    nom = models.CharField(max_length=120)
    habilitation_mini = models.ForeignKey(Habilitation, on_delete=models.CASCADE)

    def __str__(self):
        return self.nom


class Equipement(models.Model):
    nom = models.CharField(max_length=150)
    description = models.TextField("Description", blank=True, help_text="Détail de l'équipement")
    categorie = models.ForeignKey(CategorieEquipement, on_delete=models.CASCADE)

    def __str__(self):
        return self.nom

class HabiitationUtilisateur(models.Model):
    utilisateur = models.OneToOneField(User, on_delete=models.CASCADE, default=None, help_text="Utilisateur", null=True, blank=True)
    categorie = models.ForeignKey(CategorieEquipement, on_delete=models.CASCADE)
    habilitations = (
        ('0', 'Expert'),
        ('1', 'Autonome'),
        ('2', 'Accompagné'),
        ('', 'Novice')
    )
    habilitation = models.CharField(max_length=15, choices=habilitations)


