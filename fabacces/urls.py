
from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.contrib import admin
from equipement import views
from rest_framework_swagger.views import get_swagger_view

from django.contrib.auth.views import logout
from django.conf import settings
from ajax_select import urls as ajax_select_urls



schema_view = get_swagger_view(title='FabAccess')

urlpatterns = [
    url(r'^/?$', views.home, name="home"),
    url('equipement/', include('equipement.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('admin/', admin.site.urls),


    url('api/', include('api.urls')),





]
