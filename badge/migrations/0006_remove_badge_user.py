# Generated by Django 2.0.5 on 2018-06-08 13:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('badge', '0005_auto_20180605_1639'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='badge',
            name='user',
        ),
    ]
