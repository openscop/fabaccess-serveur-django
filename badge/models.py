from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Badge(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=None, help_text="Utilisateur lié au badge", null=True, blank=True)
    numero_tag = models.CharField(max_length=18, help_text="Numéro interne de la puce RFID")
    active = models.BooleanField(default=False, help_text="Badge actif ou désactivé")

    def __str__(self):
        return self.numero_tag
